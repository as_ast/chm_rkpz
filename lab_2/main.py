import sympy as sym
import numpy as np
import copy
from typing import List, Tuple


# Функция для вычисления значения системы
def eval_system(system: List, independent_argument: float, point: List) -> np.array:
    return np.array([
        system[i].subs([
            (x, independent_argument),
            (u, point[0]),
            (v, point[1]),
            (w, point[2]),
            (z, point[3])
        ]) for i in range(len(system))
    ])


# Функция для вычисления значения вспомогательной системы, строящейся в ходе работы метода Ньютона
def eval_help_system(previous_values: List, system: List, independent_argument: float, point: List) -> np.array:
    u_der, v_der, w_der, z_der = sym.symbols('u_der, v_der, w_der, z_der')
    return np.array([
        system[i].subs([
            (x, independent_argument),
            (u, previous_values[0]),
            (v, previous_values[1]),
            (w, previous_values[2]),
            (z, previous_values[3]),
            (u_der, point[0]),
            (v_der, point[1]),
            (w_der, point[2]),
            (z_der, point[3]),
        ]) for i in range(len(system))
    ])


# Функция, решающая задачу Коши
def runge_kutta_system(system: List, initial_conditions: List, integration_interval: List, step: float,
                       verbose: bool = False, previous_result: List = None) -> List:
    local_result = []
    current_x = integration_interval[0]
    current_point = initial_conditions
    index = 0
    while current_x + step < integration_interval[1]:
        if previous_result is None:
            k1 = eval_system(system, current_x, current_point)
            k2 = eval_system(system, current_x + step / 2, current_point + k1 * step / 2)
            k3 = eval_system(system, current_x + step / 2, current_point + k2 * step / 2)
            k4 = eval_system(system, current_x + step, current_point + k3 * step)
        else:
            k1 = eval_help_system(previous_result[index], system, current_x, current_point)
            k2 = eval_help_system(previous_result[index], system, current_x + step / 2, current_point + k1 * step / 2)
            k3 = eval_help_system(previous_result[index], system, current_x + step / 2, current_point + k2 * step / 2)
            k4 = eval_help_system(previous_result[index], system, current_x + step, current_point + k3 * step)
        current_point += step * (k1 + 2 * k2 + 2 * k3 + k4) / 6
        local_result.append(copy.deepcopy(current_point))
        if verbose:
            print('x =', current_x, '|| point =', current_point)
        current_x += step
        index += 1
    if previous_result is None:
        current_point += (integration_interval[1] - current_x) * eval_system(system, current_x, current_point)
    else:
        current_point += (integration_interval[1] - current_x) * eval_help_system(previous_result[-1], system,
                                                                                  current_x, current_point)
    local_result.append(copy.deepcopy(current_point))
    current_x = integration_interval[1]
    if verbose:
        print('x =', current_x, '|| point =', current_point)
    return local_result


# Реализация метода Ньютона
def newton_method(previous_result: List, phi: List, system: List, integration_interval: List, step: float) -> List:
    u_der, v_der, w_der, z_der = sym.symbols('u_der v_der w_der z_der')
    # Собираем первую вспомогательную задачу Коши
    cauche_problem_alpha = [
        (sym.diff(system[index], u) * u_der + sym.diff(system[index], v) * v_der +
         sym.diff(system[index], w) * w_der + sym.diff(system[index], z) * z_der) for index in range(len(system))
    ]
    cauche_problem_alpha_initial_conditions = [1, 0, 0, 0]
    # Собираем вторую вспомогательную задачу Коши
    cauche_problem_beta = [
        (sym.diff(system[index], u) * u_der + sym.diff(system[index], v) * v_der +
         sym.diff(system[index], w) * w_der + sym.diff(system[index], z) * z_der) for index in range(len(system))
    ]
    cauche_problem_beta_initial_conditions = [0, 1, 0, 0]
    # Решаем полученные задачи
    result_alpha = runge_kutta_system(cauche_problem_alpha, cauche_problem_alpha_initial_conditions,
                                      integration_interval, step,
                                      previous_result=previous_result)
    result_beta = runge_kutta_system(cauche_problem_beta, cauche_problem_beta_initial_conditions, integration_interval,
                                     step,
                                     previous_result=previous_result)
    # Собираем матрицу А
    matrix = [
        [result_alpha[-1][0], result_beta[-1][0]],
        [result_alpha[-1][1], result_beta[-1][1]]
    ]
    # Находим delta_alpha и delta_beta
    try:
        delta_beta = (phi[0] * matrix[1][0] / matrix[0][0] - phi[1]) / (
                matrix[1][1] - matrix[0][1] * matrix[1][0] / matrix[0][0])
        delta_alpha = (-phi[0] - matrix[0][1] * delta_beta) / matrix[0][0]
    except ZeroDivisionError:
        raise ZeroDivisionError
    return [delta_alpha, delta_beta]


# Реализация метода пристрелки для системы ОДУ
def sighting_method(system: List, border_conditions: List, initial_sighting_params: List, integration_interval: List,
                    num_points: int, threshold: int, epsilon: float, verbose: bool = False) -> Tuple[List, List]:
    step = (integration_interval[1] - integration_interval[0]) / num_points
    iterations_counter = 0
    current_sighting_params = initial_sighting_params
    current_initial_conditions = [
        current_sighting_params[0], current_sighting_params[1], border_conditions[2], border_conditions[3]
    ]
    current_result = runge_kutta_system(system, current_initial_conditions, integration_interval, step)
    phi = [current_result[-1][0] - border_conditions[0], current_result[-1][1] - border_conditions[1]]
    if abs(phi[0]) <= epsilon and abs(phi[1]) <= epsilon:
        print('Iterations passed:', iterations_counter)
        return current_result, current_sighting_params
    # Начинаем поиск нужных пристрелочных параметров
    for i in range(threshold):
        delta_sighting_params = newton_method(current_result, phi, system, integration_interval, step)
        iterations_counter += 1
        current_sighting_params = [
            current_sighting_params[0] + delta_sighting_params[0], current_sighting_params[1] + delta_sighting_params[1]
        ]
        current_initial_conditions = [
            current_sighting_params[0], current_sighting_params[1], border_conditions[2], border_conditions[3]
        ]
        current_result = runge_kutta_system(system, current_initial_conditions, integration_interval, step)
        if verbose:
            print('Iteration №', iterations_counter, sep='')
            print('Current sighting params:', current_sighting_params)
            print('Current Phi:', phi)
            print('Last point:', current_result[-1])
        phi = [current_result[-1][0] - border_conditions[0], current_result[-1][1] - border_conditions[1]]
        if abs(phi[0]) <= epsilon and abs(phi[1]) <= epsilon:
            print('Iterations passed:', iterations_counter)
            return current_result, current_sighting_params
    print('Max number of iterations exceeded')
    return [], []


if __name__ == '__main__':
    x, u, v, w, z = sym.symbols('x u v w z')

    # Первая тестовая система
    # test_system = [
    #     -v - x,
    #     u - 3,
    #     w - 3 * x - 1,
    #     z - x + 2
    # ]
    # test_border_conditions = [2.5403, -0.1585, 4, -1]
    # test_initial_sighting_params = [0, -2]
    # expected_result = [2.5403, -0.1585, 7, 0]
    # expected_params = [3, 0]

    # Вторая тестовая система
    # test_system = [
    #     2 * v + 8,
    #     z - 3 * v - 6,
    #     -(w - 10 * u + 20 * v + 50),
    #     3 * v - z + 10
    # ]
    # test_border_conditions = [9, -1, -10, 1]
    # test_initial_sighting_params = [-1, 5]
    # expected_result = [9, -1, 0, 4]
    # expected_params = [4, -2]

    # Третья тестовая система (нелинейная)
    test_system = [
        -4 / (z + 3.5 * w - 3),
        u,
        u * w,
        4 * w - 7
    ]
    test_border_conditions = [0.5, 0.65931, 2, 0]
    test_initial_sighting_params = [5, 5]
    expected_result = [0.5, 0.6931, 4, 5]
    expected_params = [1, 0]

    interval = [0, 1]
    points = 20
    eps = 1e-3
    k = 100

    result, result_params = sighting_method(test_system, test_border_conditions, test_initial_sighting_params,
                                            interval, points, k, eps, verbose=True)

    print('result: ', result[-1], ', expected: ', expected_result,
          ', \ndiff: ', [abs(result[-1][i] - expected_result[i]) for i in range(len(result[-1]))], sep='')
    print('result_params: ', result_params, ', expected_params: ', expected_params,
          ', diff: ', [abs(result_params[i] - expected_params[i]) for i in range(len(result_params))], sep='')
