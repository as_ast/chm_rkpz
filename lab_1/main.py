import numpy as np


def runge_kutta(
        right_side_function, integration_interval: np.array,
        y_0: np.array, step=1e-3, verbose=True
) -> np.array:
    current_x = integration_interval[0]
    current_y = y_0
    while current_x + step < integration_interval[1]:
        k1 = right_side_function(current_x, current_y)
        k2 = right_side_function(current_x + step / 2, current_y + k1 * step / 2)
        k3 = right_side_function(current_x + step / 2, current_y + k2 * step / 2)
        k4 = right_side_function(current_x + step, current_y + k3 * step)
        current_y += step * (k1 + 2 * k2 + 2 * k3 + k4) / 6
        current_x += step
        if verbose:
            print("x = ", current_x, "|| y = ", current_y)
    current_y += (integration_interval[1] - current_x) * right_side_function(current_x, current_y)
    current_x = integration_interval[1]
    if verbose:
        print("x = ", current_x, "|| y = ", current_y)
    return current_y


def secant_method(
        function, init_value_0: float, init_value_1: float, n_iterations: int, verbose=True,
        threshold=1e-7
) -> float:
    prev_value = init_value_0
    current_value = init_value_1
    prev_func = function(prev_value)
    current_func = function(current_value)

    if verbose:
        print("Start: value = ", current_value, ", function value = ", current_func)
    for i_iter in range(n_iterations):
        new_value = current_value - current_func * (current_value - prev_value) / (current_func - prev_func)
        prev_value = current_value
        current_value = new_value
        prev_func = current_func
        current_func = function(current_value)
        if verbose:
            print("Iteration ", i_iter, ": value = ", current_value, ", function value = ", current_func)
        if abs(current_func) < threshold:
            break
    return current_value


def targeting_by_secant_method_3(
        right_function, border_values: np.array, integration_interval: np.array,
        rg_step=1e-4, sm_n_iterations=25
):
    def rk_right_side_function(x: float, values: np.array) -> np.array:
        return np.array([values[1], values[2], right_function(x, values)])

    def secant_func(alpha: float) -> float:
        rk_result = runge_kutta(rk_right_side_function, integration_interval,
                                np.array([border_values[0], border_values[1], alpha]), rg_step, False)
        return rk_result[0] - border_values[2]

    result_alpha = secant_method(secant_func, np.mean(border_values),
                                 np.mean(border_values + np.array([0.1, 0.1, 0.1])), sm_n_iterations)
    result = runge_kutta(rk_right_side_function, integration_interval,
                         np.array([border_values[0], border_values[1], result_alpha]), rg_step, False)
    return result, result_alpha


def adams(
        right_side_function, x: float, x_0: float,
        y_0: np.array, step=1e-3, verbose=True
) -> np.array:
    k1 = right_side_function(x_0, y_0)
    k2 = right_side_function(x_0 + step / 2, y_0 + k1 * step / 2)
    k3 = right_side_function(x_0 + step / 2, y_0 + k2 * step / 2)
    k4 = right_side_function(x_0 + step, y_0 + k3 * step)
    previous_x = x_0
    previous_y = y_0
    current_x = x_0 + step
    current_y = y_0 + step * (k1 + 2 * k2 + 2 * k3 + k4) / 6
    while current_x + step < x:
        tmp_current_y = current_y

        current_y += step * (3 * right_side_function(current_x, current_y) / 2 -
                             right_side_function(previous_x, previous_y) / 2)
        previous_y = tmp_current_y
        previous_x = current_x
        current_x += step

        if verbose:
            print("x = ", current_x, "|| y = ", current_y)
    current_y += step * (3 * right_side_function(current_x, current_y) / 2 -
                         right_side_function(previous_x, previous_y) / 2)
    current_x += step
    if verbose:
        print("x = ", current_x, "|| y = ", current_y)

    return current_y


def my_function(x: float, y: np.array) -> np.array:
    div = - (y[0] * 18) / (y[1] * y[2])
    result = div + y[2] - 6 * x - 11
    return result


def my_second_function(x: float, y: np.array) -> np.array:
    return 24 * x - y[1] + x * y[2] / 3 + x * y[0] / 3 - (x ** 5) / 3


def my_third_function(x: float, y: np.array):
    return y[2] + y[1] - y[0]


def main():
    #result, result_alpha = targeting_by_secant_method_3(my_function, np.array([27, 27, 64]), np.array([0, 1]))
    #expected_result = np.array([64, 48, 24])
    #expected_alpha = 18

    #result, result_alpha = targeting_by_secant_method_3(my_second_function, np.array([1, 0, 1 + np.cos(1)]), np.array([0, 1]))
    #expected_result = np.array([1 + np.cos(1), 4 - np.sin(1), 12 - np.cos(1)])
    #expected_alpha = -1

    result, result_alpha = targeting_by_secant_method_3(my_third_function, np.array([0, 1, np.exp(1)]), np.array([0, 1]))
    expected_result = np.array([np.exp(1), 2 * np.exp(1), 3 * np.exp(1)])
    expected_alpha = 2

    print("Result: ", result, ", expected: ", expected_result, ", diff: ", result - expected_result, sep='')
    print("Result alpha: ", result_alpha, ", expected alpha: ", expected_alpha,
          ", diff: ", result_alpha - expected_alpha, sep='')


if __name__ == '__main__':
    main()
